# README


## création de l'application 

Pour le TP nous utiliserons sqlite3 (par default) .

Donc pas de précision de l'option `-d`

 
 ```
$ rails new MyBlog
```

**ATTENTION** ne pas oublier de créer une copie du fichier `config/database.yml` en par exemple `config/database.yml.sample`

et de mettre database.yml dnas le fichiers .gitignore ou dans un fichier git ignore de la machine, pour ne pas devoir le refaire à chaque projet. (cf git)

## Création de la base de données 

```
$ rails db:create
```

## création  du model `user` 

On choisi de créer le modèle user car il ne dépend poue le moment d'aucun autre model 

```
$ rails g scaffold user name:string:index email:string:index
``` 

le `:index` permet de créer des index sur les champs correspondant.

## migration 

```
$ rails db:migrate
```
en retour de console on obtient : 

``` 
== 20200325182804 CreateUsers: migrating ======================================
-- create_table(:users)
 -> 0.0029s
-- add_index(:users, :name)
 -> 0.0015s
-- add_index(:users, :email)
   -> 0.0021s
== 20200325182804 CreateUsers: migrated (0.0067s) =============================
```

ça a aussi créé le fichier `db/schema.rb`  qui reprensente notre base de données vu par ruby. 

On ne precise pas la colonne `:id`,  elle sera donc mise par défaut lors de la migration, si on ne souhaite pas en avoir on le précisera dans le fichier de migration, avant d'effectuer la migration dans la déclaration du `create_table`



## creation du model `blog` qui fera référence au model `user`

Un article du blog fera réference à un utilisateur.

```
rails g scaffold blog title:string content:text published_at:datetime draft:boolean:index user:references 
```

on remet ici un `:index` sur le champ `draft`  ça permettra de trier rapidement les publication en ligne ou pas.


le `user:references` va permettre de créer le champ `user_id` dans la table blog.

de placer une association `belongs_to` dans le model `Blog`

On effectue la migration pour `blog`

``` 
rails db:migrate
```

## Modificaiton de Blog

on modifie la vue et le controller de `blogs` pour afficher un select à la place d'un input.  

### fichier `_form.html.erb` 

changement de 

``` 
<%= form.text_field :user_id %>
```

pour 

``` 
<%= form.select :user_id, @users.collect {|a| [a.name, a.id]} %>
```

### controller blog 

on ajoute un `before_action` pour charger une instance de `User` dans le fichier `blogs_controller.rb`

``` 
before_action :set_users, only: [:new, :show, :edit, :update, :destroy]
```

 en dessous du `private` on déclare le `set_users` 

``` 
def set_users
    @users = User.all
end 
```


## fichier seed

Afin de pouvoir travailler avec un peu de data, on va utiliser le fichier seed pour remplir notre base

```
echo "gem 'faker'" >> Gemfile 
```

``` 
bundle install 
```

dans `db/seeds.rb`

```
# on boucle 30 fois pour créer un user
(1..30).each do |number|
  User.create name:   Faker::Name.name,
              email:  Faker::Internet.email
end

# on bvoulce 100 faois pour créer un article, et on utilise les users créés auparavant pour fixer le user_id
(1..100).each do |number|
  Blog.create title:        Faker::Lorem.sentence,
              content:      Faker::Lorem.paragraph(sentence_count: 5),
              published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
              draft:        rand(0..1),
              user:         User.all.sample
end
```


On lance le `seed`

```
rails db:seed
```


## query n+1

https://guides.rubyonrails.org/active_record_querying.html#eager-loading-associations

change controller blogs and users with : 

```
class BlogsController < ApplicationController
[...]
  def index
    @blogs = Blog.includes(:user).all
  end
[...]
```


``` 
class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update, :destroy]
[...]
  def show
    @user = User.includes(:blogs).find(params[:id])
  end
[..]
```

