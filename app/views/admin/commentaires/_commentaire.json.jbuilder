json.extract! commentaire, :id, :content, :name, :email, :lastname, :published_at, :blog_id, :created_at, :updated_at
json.url commentaire_url(commentaire, format: :json)
