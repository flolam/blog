class SessionController < ApplicationController
  def show
  end

  def create

    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      redirect_to admin_blogs_path
    else
      redirect_to login_url
    end

  end


  def destroy
    session[:user_id] = nil

    redirect_to blogs_url
  end
end
