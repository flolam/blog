module Admin
class SessionController < AdminController
  def show
  end

  def create

    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      session[:user_name] = user.name
      redirect_to blogs_url
    else
      redirect_to login_url
    end

  end


  def destroy
    session[:user_name] = nil
    redirect_to blogs_url
  end
end
end
