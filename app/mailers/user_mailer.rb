class UserMailer < ActionMailer::Base
  def forgot_password(user)
    @user = user
    mail from:'test@test.com', to: user.email, :subject => 'Reset password'
  end
end

