class CreateCommentaires < ActiveRecord::Migration[6.0]
  def change
    create_table :commentaires do |t|
      t.text :content
      t.string :name
      t.string :email
      t.string :lastname
      t.datetime :published_at
      t.references :blog, null: false, foreign_key: true

      t.timestamps
    end
  end
end
