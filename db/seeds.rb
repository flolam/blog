#User.delete_all if User.all.size.positive?
#(1..30).each do |number|
#User.create name:   Faker::Name.name,
#email:  Faker::Internet.email
#end

#Blog.delete_all if Blog.all.size.positive?
#(1..100).each do |number|
#Blog.create title:        Faker::Lorem.sentence,
#content:      Faker::Lorem.paragraph(sentence_count: 5),
#published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
#draft:        rand(0..1),
#user:         User.all.sample
# end

#Tag.delete_all if Tag.all.size.positive?
#(1..30).each do |number|
#Tag.create name:   Faker::Lorem.word
# end

Commentaire.delete_all if Commentaire.all.size.positive?
(1..30).each do |number|
  Commentaire.create name:         Faker::Name.name,
                     email:        Faker::Internet.email,
                     lastname:     Faker::Name.last_name,
                     content:      Faker::Lorem.sentences(2),
                     published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
                     blog:         Blog.all.sample
end