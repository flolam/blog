Rails.application.routes.draw do

  resources :password_resets
  get 'logout' => 'session#destroy'
  get 'login' => 'session#show'
  post 'login' => 'session#create'
  resources :commentaires
  resources :comments
  resources :tags
  resources :blogs
  resources :users
  root "blogs#index"

  namespace :admin do
    resources :commentaires
    resources :comments
    resources :tags
    resources :blogs
    resources :users
  end


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
